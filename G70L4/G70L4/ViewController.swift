

import UIKit

class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
//		workWithStrings()
//		workWithArrays()
//		unwrapingOptionals()
		workWithDictionaries()
	}
	
	func workWithStrings() {
		let age = 26
		let name = "Ivan"
		print(name)
		let surname = "Besarab"
		let fullName = "\(name) \(surname)"
		print(fullName + " " + name)
		print("surname is \(surname.count) characters long")
		
		
		let longStrWithErrors = """
//
//  ViewController .swift
//  G70L4 \(age)
//
//  Created by  Ivan Vasilevich on 4/11/19.
//  Copyright © 2019  RockSoft . All rights  reserved.
//
"""
		var longStr = longStrWithErrors.replacingOccurrences(of: "  ", with: " ")
		longStr = longStr.replacingOccurrences(of: " .", with: ".")
		print(longStr)
		
		let result = name.prefix(2)
		print(result)
		
		let nsLongStr = longStrWithErrors as NSString
		
//		let someInt = Int() // = 0
//		let someStr = String() // = ""
		let range = NSRange(location: 11, length: 8)
		let controllWord = nsLongStr.substring(with: range)
		print(controllWord)
		
		
	}

	func workWithArrays() {
		var array = [7, 3, 10] // Array<Int>()
		print(array)
		print(array.count)
		print(array[2])
		array[2] = 11
		array.remove(at: 0)
		// array[array.count - 1]
		array.append(15)
		array.append(23)
		print(array.firstIndex(of: 15)!)
		
		array.removeAll()
		
		for _ in 0..<20 {
			let randomNumber = Int.random(in: 0...9)
			array.append(randomNumber)
		}
		print(array)
		
		for i in 0..<array.count {
			let elementOffArray = array[i]
			print("index \(i) \t\t\(elementOffArray)")
		}
		
		let colors = ["red",
					  "blue",
					  "yellow",
					  "green"]
		
		let indexOfYellow = colors.firstIndex(of: "yellow")
		print("indexOfYellow \(indexOfYellow!)")
		
	}
	
	func unwrapingOptionals() {
		var array = [4, 7, 11]
		if array.isEmpty {
			print("no elements in array")
			return
		}
		array.removeAll()
		let optionalFirst = array.first
		let numberFromString = Int("35")
		let last = 5
		
		//optional binding
		if let realFirstElement = optionalFirst {
			let sumOfFirstAndLastNumbers = realFirstElement + last
			print("sumOfFirstAndLastNumbers = \(sumOfFirstAndLastNumbers)")
		}
		else {
			print("no elements in array")
		}
		
		//default value -- nil coalescing
		var sumOfFirstAndLastNumbers = (optionalFirst ?? 0) + last
		
		//force unwrap
		sumOfFirstAndLastNumbers = optionalFirst! + last
		print("sumOfFirstAndLastNumbers = \(sumOfFirstAndLastNumbers)")
		
		guard let realFirstElement = optionalFirst else {
			print("no elements in array")
			return
		}
		guard let realNumberFromStr = numberFromString else {
			print("no elements in array")
			return
		}
		
		print(numberFromString! + realFirstElement + realNumberFromStr)
		
	}
	
	func workWithDictionaries() {
		var carBook = [
			"HachiRoku" : "AE86",
			"Tabletka" : "UAZ",
			"Шишига" : "GAZ66",
			"Sliva" : "Silvia",
			"Фашка" : "240Z"
		]
		let shishka = carBook["aШишига"]
		let otmaza = "я не знаю"
		
		print("shishka = \(shishka ?? otmaza)")
		carBook["ШишигаShort"] = "66"
		carBook.removeValue(forKey: "dadsda")
		
		for pair in carBook {
			print(pair.key)
		}
	}
	
}

