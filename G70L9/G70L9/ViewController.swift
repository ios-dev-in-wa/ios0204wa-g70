//
//  ViewController.swift
//  G70L9
//
//  Created by Ivan Vasilevich on 5/2/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	@IBOutlet weak var nicknameTextField: UITextField!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		nicknameTextField.placeholder = "nickname"
		nicknameTextField.delegate = self
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//		nicknameTextField.resignFirstResponder()
		view.endEditing(true)
	}

}

extension ViewController: UITextFieldDelegate {
	
	func textFieldDidBeginEditing(_ textField: UITextField) {
		print("textFieldDidBeginEditing")
	}
}

