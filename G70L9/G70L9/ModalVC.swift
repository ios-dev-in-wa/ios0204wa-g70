//
//  ModalVC.swift
//  G70L9
//
//  Created by Ivan Vasilevich on 5/2/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class ModalVC: UIViewController {

	@IBOutlet weak var tableView: UITableView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		tableView.delegate = self
	}
	
	var items = ["String0", "Str1", "String2", "Str3"]
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		dismiss(animated: true, completion: nil)
		navigationController?.popToRootViewController(animated: true)
	}
}

extension ModalVC: UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return items.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "redCell", for: indexPath)
		cell.textLabel?.text = items[indexPath.row]
		return cell
	}
}

extension ModalVC: UITableViewDelegate {
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		print(items[indexPath.row])
	}
}
