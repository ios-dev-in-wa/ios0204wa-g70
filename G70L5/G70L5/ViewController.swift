//
//  ViewController.swift
//  G70L5
//
//  Created by Ivan Vasilevich on 4/16/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.

		//		"HachiRoku" : "AE86",
		//		"Tabletka" : "UAZ",
		//		"Шишига" : "GAZ66",
		//		"Sliva" : "Silvia",
		//		"Фашка" : "240Z"
		
		let car1 = Car()
		car1.name = "HachiRoku"
		car1.color = "red"
		car1.maxSpeed = 180
//		car1.currentSpeed = 100500
		car1.accelerate()
		print(car1)
//		car1.speed = 100500
		
		let car2 = Car.init()
		car2.name = "Tabletka"
		car2.color = "boloto-gold"
		car2.maxSpeed = 100
		print(car2.description)
		
		let car3 = Car(name: "Шишига", color: "", maxSpeed: 99)
		car3.color = "boloto-green"
		car3.maxSpeed = 80
		print(car3.description)
		

	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		drawBox()
	}
	
	func drawBox() {
		let box = UIView()
		box.frame.size.width = 100
		box.frame.size.height = 180
		box.frame.origin.x = 50
		box.frame.origin.y = 150
		box.backgroundColor = .cyan
		view.addSubview(box)
	}


}

