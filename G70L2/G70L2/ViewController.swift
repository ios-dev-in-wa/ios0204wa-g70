//
//  ViewController.swift
//  G70L2
//
//  Created by Ivan Vasilevich on 4/4/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		doNothing()
		varsAndLets()
		easyChose()
	}
	
	func doNothing() {
		
	}
	
	func varsAndLets() {
		let speedOfLight = 299996
		print("speedOfLiht: \(speedOfLight) km/s")
		
		var mySpeed = 299.0
		print("speed: \(mySpeed) km/s")
		mySpeed = 3705 / 3600
		print("speed: \(mySpeed) km/s")
		
		let differenceBetwenMySpeedAndSpeedOfLight = Double(speedOfLight) - mySpeed
		print("difference Betwen My Speed And Speed Of Light: \(differenceBetwenMySpeedAndSpeedOfLight) km/s")
	}
	
	func easyChose() {
		let candyNumber = Int.random(in: 1...13)
		print("winner is #\(candyNumber)")
	}
	
	func namespace() {
		let a = 5
		print(a)
		for _ in 0..<5 {
			print(a)
			let b = a + 3
			print(b)
		}
//		print(b)
		print(a)
	}
	
	func unaryOperators() {
		let kabanc = 7
		let minusC = -kabanc //  a - v - +c - d
		 _ = minusC + kabanc + kabanc + kabanc + kabanc + kabanc + +kabanc + kabanc + kabanc + kabanc + kabanc + kabanc + kabanc + kabanc + kabanc + kabanc
//		print(sum)
		
		let frontIsBlocked = true
		let frontIsClear = !frontIsBlocked
		
		print(frontIsClear)
	}
	
	func binaryOperators() {
//		arithmetic
//		+ - * / %
//		16 % 3 + 2
		var dengi = 5
		dengi = dengi + 1
		dengi += Int.random(in: 1...3)
		
//		logic
//		< > <= >= == !=
		let denegMalo = dengi < 33999
		let young = Int.random(in: 1...3) < 40
//		and && / || or
		if !denegMalo && young {
			print("lovi konfetu!")
		}
		else {
			print("idi rabotat6")
		}
		
	}
	
	

}

