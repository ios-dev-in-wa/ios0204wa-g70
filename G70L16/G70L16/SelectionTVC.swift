//
//  SelectionTVC.swift
//  G70L16
//
//  Created by Ivan Vasilevich on 5/31/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class SelectionTVC: UITableViewController {
	
	weak var delegate: TVCSelectionDelegate?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		print("prrr2")
	}

    // MARK: - Table view data source

	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		delegate?.didSelectItemAt(indexPath)
	}
}

protocol TVCSelectionDelegate: NSObject  {
	func didSelectItemAt(_ indexPath: IndexPath)
}
