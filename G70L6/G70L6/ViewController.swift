//
//  ViewController.swift
//  G70L6
//
//  Created by Ivan Vasilevich on 4/23/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	@IBOutlet weak var counterLabel: UILabel!
	@IBOutlet weak var imageView: UIImageView!
	let kCount = "odfjafhdsJKJDSsjdksaljdege"
	
	var count: Int {
		return UserDefaults.standard.integer(forKey: kCount)
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		let car1 = Car()
		car1.accelerate()
		print(car1.color)
		
	}

	@IBAction func firstButtonPressed() {
		view.backgroundColor = UIColor(red: CGFloat(173)/255,
									   green: CGFloat(150)/255,
									   blue: CGFloat(150)/255,
									   alpha: 1)
		
		view.backgroundColor = UIColor.init(rgb: 0xE238CC)
//		view.backgroundColor = UIColor(displayP3Red: <#T##CGFloat#>, green: <#T##CGFloat#>, blue: <#T##CGFloat#>, alpha: <#T##CGFloat#>)

	}
	
	@IBAction func secondButtonPressed() {
		
		UserDefaults.standard.set(count + 1, forKey: kCount)
		
		counterLabel.text = count.description
	}
	
	@IBAction func changeColor(_ sender: UIButton) {
		print(sender.currentTitle!)
		
		switch sender.tag {
		case 0:
			view.backgroundColor = .red
			imageView.image = UIImage(named: "Rick and Morty")
		case 1:
			view.backgroundColor = .yellow
			imageView.image = UIImage(named: "Rick and Morty2")
		case 2:
			view.backgroundColor = .green
			imageView.image = UIImage(named: "Rick and Morty3")
		default:
			fatalError("KUKU YOPTA")
		}
		
	}
}

