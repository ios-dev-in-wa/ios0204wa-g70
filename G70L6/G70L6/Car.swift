//
//  Car.swift
//  G70L5
//
//  Created by Ivan Vasilevich on 4/16/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class Car: NSObject {
	
	var name = "unknown"
	var color = "unknown"
	var maxSpeed = 0
	
	var speed: Int {
		return currentSpeed
	}
	
	override var description: String {
		return "\(name), color: \(color), maxSpeed: \(maxSpeed)km/h, currentSpeed: \(currentSpeed)km/h" + super.description
	}
	
	private var currentSpeed = 0
	
	override init() {
		
	}
	
	init(name: String, color: String, maxSpeed: Int) {
		self.name = name
	}
	
	func accelerate() {
		if currentSpeed + 10 < maxSpeed {
			currentSpeed += 10
		}
		else {
			currentSpeed = maxSpeed
		}
		//		color = (currentSpeed + 10 < maxSpeed) ? "green" : "red"
		//		currentSpeed + 10 < maxSpeed ? currentSpeed += 10 : currentSpeed = maxSpeed
	}
	
	
	
}
