//
//  ViewController.swift
//  G70L11
//
//  Created by Ivan Vasilevich on 5/14/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class SecondVC: UIViewController {
	
	@IBOutlet weak var floatingButton: UIButton!
	@IBOutlet weak var buttonsStackView: UIStackView!
	var action: () -> Void = {}
	var a = 5
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		title = a.description
	}
	
	override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
		if UIDevice.current.orientation.isLandscape {
			print("landscape")
			buttonsStackView.axis = .horizontal
		} else {
			print("portrait")
			buttonsStackView.axis = .vertical
		}
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		action()
//		dismiss(animated: true) {
//			print("SecondVC Dropped")
//		}
		
		let loc = touches.first?.location(in: view) ?? .zero
		
		UIView.animate(withDuration: 01.25) {
			
		}
		
		UIView.animate(withDuration: 3,
					   delay: 2,
					   usingSpringWithDamping: 0.5,
					   initialSpringVelocity: 0.5,
					   options: [.repeat, .autoreverse],
					   animations: {
						self.floatingButton.center = loc
//						self.floatingButton.alpha = 0.4
		}) { (completed) in
			print("animation completed:", completed)
		}
		
	}

	@IBAction func changeStackAxis() {
		let isStackHorizontal = buttonsStackView.axis == .horizontal
		
		UIView.transition(with: buttonsStackView,
						  duration: 2,
						  options: .transitionCurlUp,
						  animations: {
							self.buttonsStackView.axis = isStackHorizontal ? .vertical : .horizontal
		},
						  completion: nil)
	}
	
}

