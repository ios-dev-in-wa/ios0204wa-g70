//
//  FirstVC.swift
//  G70L11
//
//  Created by Ivan Vasilevich on 5/14/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class FirstVC: UIViewController {

//	http://fuckingclosuresyntax.com/
	

	let source = """
https://regexr.com/
RegExr was created by gskinner.com, and is proudly hosted by Media Temple.

Edit the Expression & Text to see matches. Roll over matches or the expression for details. PCRE & Javascript flavors of RegEx are supported.
0.07 123.45678
The side bar includes a Cheatsheet, full Reference, and Help. You can also Save & Share with the Community, and view patterns you create or favorite in My Patterns.

Explore results with the Tools below. Replace & List output custom results. Details lists capture groups. Explain describes your expression in plain English.

"""

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
		guard let secondVC = segue.destination as? SecondVC else {
			return
		}
		secondVC.a = 7
		secondVC.action = {
			self.paintMeInRed()
		}
    }
	
	private func paintMeInRed() {
		view.backgroundColor = .red
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		
		print(matches(for: "[0-9]{0,4}\\.[0-9]{2}", in: source))
		
		let alert = UIAlertController(title: "Rrrrrrrrr", message: """
http://fuckingclosuresyntax.com/
http://fuckingclosuresyntax.com/
http://fuckingclosuresyntax.com/
""", preferredStyle: .alert)
		
		alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
		
		alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
			print("password is:\"\(alert.textFields!.first!.text!)\"")
		}))
		
		alert.addTextField { (passwordTextField) in
			passwordTextField.placeholder = "будь"
			passwordTextField.isSecureTextEntry = true
		}
		
		present(alert, animated: true, completion: nil)
	}
	
	func matches(for regex: String, in text: String) -> [String] {
		
		do {
			let regex = try NSRegularExpression(pattern: regex)
			let results = regex.matches(in: text,
										range: NSRange(text.startIndex..., in: text))
			return results.map {
				String(text[Range($0.range, in: text)!])
			}
		} catch let error {
			print("invalid regex: \(error.localizedDescription)")
			return []
		}
	}


}
