//
//  ViewController.swift
//  G70L7
//
//  Created by Ivan Vasilevich on 4/25/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	
	@IBOutlet weak var redView: UIView!
	
	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
		super.init(nibName: nibNameOrNil,
				   bundle: nibBundleOrNil)
		print("chik")
		log()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
	
	override func awakeFromNib() { //xib // nib
		super.awakeFromNib()
		log()
		print("-------------1-------------------")
		print("redView == nil", redView == nil)
	}
	
	override func viewDidLoad() {
//		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		log()
		print("-------------2-------------------")
		print("redView == nil", redView == nil)
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		log()
	}
	
	override func viewWillLayoutSubviews() {
		super.viewWillLayoutSubviews()
		log()
	}
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		log()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		log()
		//		tabBarItem.badgeValue = ""
		navigationController?.tabBarItem.badgeValue = nil
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		log()
	}
	
	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		log()
		redView.isHidden = true
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
		log()
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		guard segue.identifier == "showDrink" else {
			return
		}
		guard let drinkFromSender = sender as? Drink else {
			return
		}
		guard let drinkDisplayVC = segue.destination as? DrinkDisplayVC else {
			return
		}
		
		drinkDisplayVC.drink = drinkFromSender
		drinkDisplayVC.prevVC = self
		
	}
	
	@IBAction func drinkButtonPressed(_ sender: UIButton) {
		
		let drinks = [Drink(name: "Coffe", descr: "Awesome"),
		Drink(name: "Tea", descr: "Gorgeous")]
		let drinkToDisplay = drinks[sender.tag]
		print(drinkToDisplay)
		performSegue(withIdentifier: "showDrink", sender: drinkToDisplay)
		
		let arr = [""]
		let nsArr = arr as NSArray
		let stringFromNsArr = nsArr.object(at: 0) as! String
		print(stringFromNsArr)
	}
	
	@IBAction func aboutButtonPressed() {
		performSegue(withIdentifier: "showAbout", sender: nil)
	}
	

}

