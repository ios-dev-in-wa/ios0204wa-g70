//
//  Drink.swift
//  G70L7
//
//  Created by Ivan Vasilevich on 4/25/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import Foundation

struct Drink {
	let name: String
	let descr: String
}
