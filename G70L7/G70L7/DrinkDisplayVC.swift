//
//  DrinkDisplayVC.swift
//  G70L7
//
//  Created by Ivan Vasilevich on 4/25/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class DrinkDisplayVC: UIViewController {
	
	@IBOutlet weak var descriptionLabel: UILabel!
	
	var drink: Drink!
	weak var prevVC: ViewController!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		view.backgroundColor = .black
		
		navigationItem.title = drink.name
		descriptionLabel.text = drink.descr
    }
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		prevVC.redView.backgroundColor = .black
		prevVC.redView.isHidden = false
		navigationController?.popViewController(animated: true)
	}
	
}
