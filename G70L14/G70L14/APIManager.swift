//
//  APIManager.swift
//  G70L14
//
//  Created by Ivan Vasilevich on 5/23/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class APIManager: NSObject {
	
	typealias ResultClosure = ([Pet]) -> Void
	typealias BoolResultClosure = (Bool) -> Void
	
	static let shared = APIManager()
	
	func getPets(copletion: @escaping ResultClosure) {
		
		let sessionConfig = URLSessionConfiguration.default
		
		/* Create session, and optionally set a URLSessionDelegate. */
		let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
		
		/* Create the Request:
		PetsList (GET https://parseapi.back4app.com/classes/Pet/)
		*/
		
		guard var URL = URL(string: "https://parseapi.back4app.com/classes/Pet/") else {return}
		let URLParams = [
			"order": "-createdAt",
		]
		URL = URL.appendingQueryParameters(URLParams)
		var request = URLRequest(url: URL)
		request.httpMethod = "GET"
		
		
		// Headers
		
		request.addValue("V0HgM10nXtgiYWXVepmu0kYudeVy6vitrPi5HIPl", forHTTPHeaderField: "X-Parse-REST-API-Key")
		request.addValue("Hlsj6kBVMzZnDINZyGk3w9ViLoSSJ6GJEZTi3wpN", forHTTPHeaderField: "X-Parse-Application-Id")
		
		/* Start a new Task */
		let task = session.dataTask(with: request, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) -> Void in
			if (error == nil) {
				// Success
				let statusCode = (response as! HTTPURLResponse).statusCode
				print("URL Session Task Succeeded: HTTP \(statusCode)")
				let result = try! JSONDecoder().decode(PetResults.self, from: data!)
				print(result.results.first?.name ?? "HTTP")
				DispatchQueue.main.async {
					copletion(result.results)
				}
			}
			else {
				// Failure
				print("URL Session Task Failed: %@", error!.localizedDescription);
			}
		})
		task.resume()
		session.finishTasksAndInvalidate()
	}
	
	func postPet(pet: Pet, copletion: BoolResultClosure? = nil) {
		let sessionConfig = URLSessionConfiguration.default
		
		/* Create session, and optionally set a URLSessionDelegate. */
		let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
		
		/* Create the Request:
		CreatePet (POST https://parseapi.back4app.com/classes/Pet/)
		*/
		
		guard let URL = URL(string: "https://parseapi.back4app.com/classes/Pet/") else {return}
		var request = URLRequest(url: URL)
		request.httpMethod = "POST"
		
		// Headers
		
		request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
		request.addValue("V0HgM10nXtgiYWXVepmu0kYudeVy6vitrPi5HIPl", forHTTPHeaderField: "X-Parse-REST-API-Key")
		request.addValue("Hlsj6kBVMzZnDINZyGk3w9ViLoSSJ6GJEZTi3wpN", forHTTPHeaderField: "X-Parse-Application-Id")
		
		// JSON Body
		let jsonEncoder = JSONEncoder()
		let jsonData = try! jsonEncoder.encode(pet)
		let json = try! JSONSerialization.jsonObject(with: jsonData, options: []) as? [String: Any]
		//String(data: jsonData, encoding: String.Encoding.utf16)
		let bodyObject = json
		request.httpBody = try! JSONSerialization.data(withJSONObject: bodyObject, options: [])
		
		/* Start a new Task */
		let task = session.dataTask(with: request, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) -> Void in
			if (error == nil) {
				// Success
				let statusCode = (response as! HTTPURLResponse).statusCode
				print("URL Session Task Succeeded: HTTP \(statusCode)")
				DispatchQueue.main.async {
					copletion?(true)
				}
			}
			else {
				// Failure
				print("URL Session Task Failed: %@", error!.localizedDescription);
				DispatchQueue.main.async {
					copletion?(false)
				}
			}
		})
		task.resume()
		session.finishTasksAndInvalidate()
	}

}


class Pet: Codable {
	var objectId: String?
	var name: String?
	var type: String?
	var deviceId: String?
}

class PetResults: Codable {
	var results: [Pet]
}


protocol URLQueryParameterStringConvertible {
	var queryParameters: String {get}
}

extension Dictionary : URLQueryParameterStringConvertible {
	/**
	This computed property returns a query parameters string from the given NSDictionary. For
	example, if the input is @{@"day":@"Tuesday", @"month":@"January"}, the output
	string will be @"day=Tuesday&month=January".
	@return The computed parameters string.
	*/
	var queryParameters: String {
		var parts: [String] = []
		for (key, value) in self {
			let part = String(format: "%@=%@",
							  String(describing: key).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!,
							  String(describing: value).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
			parts.append(part as String)
		}
		return parts.joined(separator: "&")
	}
	
}

extension URL {
	/**
	Creates a new URL by adding the given query parameters.
	@param parametersDictionary The query parameter dictionary to add.
	@return A new URL.
	*/
	func appendingQueryParameters(_ parametersDictionary : Dictionary<String, String>) -> URL {
		let URLString : String = String(format: "%@?%@", self.absoluteString, parametersDictionary.queryParameters)
		return URL(string: URLString)!
	}
}
