//
//  DetailViewController.swift
//  G70L14
//
//  Created by Ivan Vasilevich on 5/23/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

	@IBOutlet weak var detailDescriptionLabel: UILabel!


	func configureView() {
		// Update the user interface for the detail item.
		if let detail = detailItem {
		    if let label = detailDescriptionLabel {
		        label.text = detail.name! + " " + detail.type!
		    }
		}
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		configureView()
	}

	var detailItem: Pet? {
		didSet {
		    // Update the view.
		    configureView()
		}
	}


}

