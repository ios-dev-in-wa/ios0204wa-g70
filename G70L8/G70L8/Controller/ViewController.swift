//
//  ViewController.swift
//  G70L8
//
//  Created by Ivan Vasilevich on 4/30/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		 view.backgroundColor = .yellow
		
		let face2 = FaceView(frame: CGRect(x: 10, y: 100, width: 128, height: 128))
//		face2.backgroundColor = .blue
//		view.addSubview(face2)
		face2.changeHappyFace(withHappLevel: 30, andColor: .blue)
		foo()
	}
	
	@IBAction func panRecognized(_ sender: UIPanGestureRecognizer) {
		let position = sender.location(in: view)
		let roboView = view.viewWithTag(55)
		roboView?.center = position
	}
	
	@IBAction func eyeBlinkTapped(_ sender: UITapGestureRecognizer) {
		let viewTag = sender.view?.tag ?? 0
		if viewTag == 0 {
			sender.view?.backgroundColor = UIColor.white.withAlphaComponent(0.25)
			sender.view?.tag = 1
		}
		else {
			sender.view?.backgroundColor = UIColor.white.withAlphaComponent(1)
			sender.view?.tag = 0
		}
	}
	
	@IBAction func paintBacteria(_ sender: UILongPressGestureRecognizer) {
		print("bakteria v zdanii", Date())
		sender.view?.backgroundColor = .green
	}
	
	func foo() {
		var myBucket1 = Bucket()
		myBucket1.candyCount = 33
		var myBucket2 = myBucket1
		myBucket2.candyCount = 0
		print("bucket struct", myBucket1)
		
		let myBag1 = Bag()
		myBag1.candyCount = 11
		let myBag2 = myBag1
		myBag2.candyCount = 2
		print("bag class", myBag1.candyCount)
		change(bag: myBag2)
		print("bag class", myBag1.candyCount)
		
	}
	
	func change(bucket: Bucket) {
//		bucket.candyCount = 3
	}
	
	func change(bag: Bag) {
		bag.candyCount = 555
	}
}

struct Bucket {
	var candyCount: Int = 0
}

class Bag {
	var candyCount: Int = 0
}
