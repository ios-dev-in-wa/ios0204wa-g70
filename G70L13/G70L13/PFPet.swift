//
//  PFPet.swift
//  G70L13
//
//  Created by Ivan Vasilevich on 5/21/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit
import Parse

class PFPet: PFObject, PFSubclassing {
	
	@NSManaged var name: String?
	@NSManaged var type: String?
	@NSManaged var owner: PFUser?
	@NSManaged var pictureFile: PFFileObject?
	
	static func parseClassName() -> String {
		return "Pet"
	}
	

}
