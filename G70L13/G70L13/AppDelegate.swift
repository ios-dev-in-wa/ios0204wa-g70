//
//  AppDelegate.swift
//  G70L13
//
//  Created by Ivan Vasilevich on 5/21/19.
//  Copyright © 2019 RockSoft. All rights reserved.
// w890520@nwytg.net

import UIKit
import Parse

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UISplitViewControllerDelegate {

	var window: UIWindow?


	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		// Override point for customization after application launch.
		let splitViewController = window!.rootViewController as! UISplitViewController
		let navigationController = splitViewController.viewControllers[splitViewController.viewControllers.count-1] as! UINavigationController
		navigationController.topViewController!.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem
		splitViewController.delegate = self
		
		initParse()
		
		return true
	}

	// MARK: - Split view

	func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController:UIViewController, onto primaryViewController:UIViewController) -> Bool {
	    guard let secondaryAsNavController = secondaryViewController as? UINavigationController else { return false }
	    guard let topAsDetailController = secondaryAsNavController.topViewController as? DetailViewController else { return false }
	    if topAsDetailController.detailItem == nil {
	        // Return true to indicate that we have handled the collapse by doing nothing; the secondary controller will be discarded.
	        return true
	    }
	    return false
	}
	
	private func initParse() {
		
		PFPet.registerSubclass()
		
		let configuration = ParseClientConfiguration { (config) in
			config.applicationId = "Hlsj6kBVMzZnDINZyGk3w9ViLoSSJ6GJEZTi3wpN"
			config.clientKey = "8iSeBkvJeKKRp8oKju1hStCE7dkAE0w9YEQrzWfC"
			config.server = "https://parseapi.back4app.com"
		}
		Parse.initialize(with: configuration)
		
		PFConfig.getInBackground()
		
	}
	

}

