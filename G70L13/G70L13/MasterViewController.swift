//
//  MasterViewController.swift
//  G70L13
//
//  Created by Ivan Vasilevich on 5/21/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit
import Parse
import SDWebImage

class MasterViewController: UITableViewController {

	var detailViewController: DetailViewController? = nil
	var objects = [PFPet]()

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		navigationItem.leftBarButtonItem = editButtonItem

		let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(insertNewObject(_:)))
		navigationItem.rightBarButtonItem = addButton
		if let split = splitViewController {
		    let controllers = split.viewControllers
		    detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
		}
	}

	override func viewWillAppear(_ animated: Bool) {
		clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
		super.viewWillAppear(animated)
		fetchObjects()
		
		if PFUser.current() != nil {
			performSegue(withIdentifier: "showAuth", sender: nil)
		}
	}
	
	private func fetchObjects() {
		let query = PFPet.query()!//PFQuery(className: "Pet")
		query.order(byDescending: "createdAt")
//		query.whereKey("name", contains: "o")
//					query.whereKey("type", equalTo: "Cat")
		query.findObjectsInBackground { (objects, error) in
			if let realObjects = objects as? [PFPet] {
				self.objects = realObjects
				self.tableView.reloadData()
//				realObjects.forEach({ (pet) in
//					print(pet.name ?? "no name")
//				})
			}
		}
		
	}

	@objc
	func insertNewObject(_ sender: Any) {
		
		let image = UIImage(named: "\(Int.random(in: 0...6))")!
		let data = image.pngData()!
		let file = PFFileObject(data: data, contentType: "png")
		file.saveInBackground { (success, error) in
			let newPet = PFPet()
			newPet.name = "Hamster"
			newPet.type = "Hamster"
			newPet.owner = PFUser.current()
			newPet.pictureFile = success ? file : nil
			newPet["deviceId"] = UIDevice.current.identifierForVendor!.description
			newPet.saveEventually { (success, error) in
				if !success {
					print(error!.localizedDescription)
				}
				else {
					self.fetchObjects()
					PFUser.current()?.relation(forKey: "pets").add(newPet)
					PFUser.current()?.saveEventually()
				}
			}
		}
		
		
	}

	// MARK: - Segues

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "showDetail" {
		    if let indexPath = tableView.indexPathForSelectedRow {
		        let object = objects[indexPath.row] 
		        let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
		        controller.detailItem = object
		        controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
		        controller.navigationItem.leftItemsSupplementBackButton = true
		    }
		}
	}

	// MARK: - Table View

	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return objects.count
	}

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

		let object = objects[indexPath.row]
		cell.textLabel!.text = object.name
//		cell.imageView?.sd_setImage(with: URL(string: object.pictureFile?.url ?? ""), completed: nil)
		cell.imageView?.image = UIImage(named: "5")
		if let link = object.pictureFile?.url {
		cell.imageView?.sd_setImage(with: URL(string: link), completed: { (image, error, type, url) in
			print(object.pictureFile?.url)
			print("--------error:", error)
		})
		}
		

		return cell
	}

	override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
		// Return false if you do not want the specified item to be editable.
		return true
	}

	override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
		if editingStyle == .delete {
		    objects.remove(at: indexPath.row)
		    tableView.deleteRows(at: [indexPath], with: .fade)
		} else if editingStyle == .insert {
		    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
		}
	}


}

