//
//  DetailViewController.swift
//  G70L13
//
//  Created by Ivan Vasilevich on 5/21/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit
import Parse

class DetailViewController: UIViewController {

	@IBOutlet weak var detailDescriptionLabel: UILabel!


	func configureView() {
		// Update the user interface for the detail item.
		if let detail = detailItem {
		    if let label = detailDescriptionLabel {
		        label.text = detail.type! + " " + detail.name!
		    }
		}
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		configureView()
		print(PFConfig.current().object(forKey: "Greeting"))
	}

	var detailItem: PFPet? {
		didSet {
		    // Update the view.
		    configureView()
		}
	}


}

