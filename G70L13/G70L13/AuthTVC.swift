//
//  AuthTVC.swift
//  G70L13
//
//  Created by Ivan Vasilevich on 5/21/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit
import Parse

class AuthTVC: UITableViewController {
	
	@IBOutlet var emailTextField: UITextField!
	@IBOutlet var passwordTextField: UITextField!

	@IBOutlet var widthsConstraints: [NSLayoutConstraint]!
	
	override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		
		var maxValue = CGFloat()
		for constraint in widthsConstraints {
			maxValue = max(constraint.constant, maxValue)
		}
		for constraint in widthsConstraints {
			constraint.constant = maxValue
		}
	}
	
	@IBAction func registerPressed(_ sender: UIBarButtonItem) {
		let user = PFUser()
		user.username = emailTextField.text!
		user.password = passwordTextField.text!
		user.email =  emailTextField.text!
		
		user.signUpInBackground { (succeeded, error) in
			if let error = error {
				let errorString = error.localizedDescription				// Show the errorString somewhere and let the user try again.
				print(errorString)
			} else {
				// Hooray! Let them use the app now.
				self.navigationController?.popViewController(animated: true)
			}
		}
	}
	
	


}
