//
//  ViewController.swift
//  G70L3
//
//  Created by Ivan Vasilevich on 4/9/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
//		ifElseSwitch()
//		loops()
		
		for i in 0..<5 {
//			squreOf(number: i)
			print(squreOfNumber(i))
		}
		
//		sum(3, and: 5)
		superMath()
		
	}
	
	func ifElseSwitch() {
		let winner = Int.random(in: 1...7)
		
		print("else - if")
		if winner == 1 {
			print("winner is Konstantin")
		}
		else if winner == 2 {
			print("winner is Roma")
		}
		else if winner == 3 {
			print("winner is Roma2")
		}
		else if winner == 4 {
			print("winner is Anton")
		}
		else {
			print("winner is #\(winner)")
		}
		
//		if winner < 3 && winner > 0 {
//
//		}
		
		print("switch")
		switch winner {
		case 1:
			print("winner is Konstantin")
		case 2:
			print("winner is Roma")
		case 3:
			print("winner is Roma2")
		case 4:
			print("winner is Anton")
		default:
			print("winner is #\(winner)")
		}
	}
	
	func loops() {
		
		for i in 0..<10 {
			for j in 0..<10 {
				print("i: \(i)\tj: \(j)")
			}
		}
		
		var isYoung = true
		var age = 15
		
		while isYoung {
			age += 1
			if age == 30 {
				continue
			}
			print("happy birthday #\(age)")
			isYoung = age < 60
		}
		
		print("WASTED")
	}

	func squreOfNumber(_ number: Int) -> Int {
		let result = number * number
		print("squreOfNumber \(number) = \(result)")
		return result
	}
	
	func sum(_ numA: Int, and  numB: Int) {
		let result = numA + numB
		print("\(numA) + \(numB) = \(result)")
	}
	
	func genRandomNumber() -> Int {
		let randomNumber = Int.random(in: 0..<10)
		print("your number is \(randomNumber)")
		return randomNumber
	}
	
	func superMath() {
		let firstNumber = genRandomNumber()
		let secondNumber = genRandomNumber()
		
		let sqOfFirstNumber = squreOfNumber(firstNumber)
		let sqOfSecNumber = squreOfNumber(secondNumber)
		
		sum(sqOfFirstNumber, and: sqOfSecNumber)
		
		checkAdoult(age: 1)
		
	}
	
	func checkAdoult(age: Int) -> Void {
		if age < 18 {
			print("podrosti")
			return
		}
		
		print("welcome")
	}

}

