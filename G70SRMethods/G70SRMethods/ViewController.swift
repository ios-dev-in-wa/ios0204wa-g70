//
//  ViewController.swift
//  G70SRMethods
//
//  Created by Ivan Vasilevich on 4/16/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		sumOdd(number: 4)
	}
	
	/*
	1. Записать метод которий виведет в консоль сообщение С приветсвием "Привет” (без аргументов) - 5мин -макс
	
	2. Записать метод которий будет выводить в консоль сообщение какой день недели относительно числа которое попадает в метод как аргумент (1 аргумент) 10 мин -макс
	
	3. Записать метод которий выводит в консоль суму двух чисел  (2 аргумента) 10 мин -макс
	
	4. Записать метод которий будет умножать задание число на 30 прибавлять 5 и возвращать результат вичислений, єтот результат нужно использовать в методе viewDidLoad для вивода в консоль  (1 аргумент + return) 15мин -макс
	
	5. Записать метод которий виведет в консоль сообщение С приветсвием "Привет” заданное количество раз (1 аргумент) - 5мин -макс
	
	6. Написать метод который будет показывать число Фибоначчи например fib(num:6) должно вернуть число 8 (1 аргумент + return)
	￼
	7. Написать метод который принимает число N и показывает сумму первых N нечетных чисел, например oddSum(n:4) = (1 + 3 + 5 + 7) должно вернуть число 16 (1 аргумент + return)
	*/
	
	func sayHello() {
		print("Hello")
	}
	
	func showDayOfWeek(dayNum: Int) {
		let days = [
			"Sun",
			"Mon",
			"Tue",
			"Wed",
			"Thu",
			"Fri",
			"Sat"
		]
		print(days[dayNum % 7])
	}
	
	func sumOfnumberA(_ a: Int, and numberB: Int) {
		let result = a + numberB
		print("sumOf\(a) and \(numberB) = \(result)")
	}
	
	func multyplyBy30Andadd5(number: Int) -> Int {
		let result = number * 30 + 5
		return result
	}
	
	func sayHello(numOfTimes: Int) {
		for _ in 0..<numOfTimes {
			sayHello()
		}
	}
	
	func fib(iteration: Int) -> Int {
		var numA = 0
		var numB = 1
		var result = numA + numB
		for _ in 0..<(iteration - 1) {
			result = numA + numB
			numA = numB
			numB = result
		}
		return result
	}
	
	func sumOdd(number: Int) {
		var sum = 0
		for i in 0...number {
			if i % 2 == 1 {
				sum += i
			}
		}
		print(sum)
	}
	
}

