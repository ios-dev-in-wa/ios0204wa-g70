//
//  ImagePresentorVC.swift
//  G70L12
//
//  Created by Ivan Vasilevich on 5/16/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit
import SDWebImage

class ImagePresentorVC: UIViewController {

	@IBOutlet weak var imageView: UIImageView!
	
	override func viewDidLoad() {
        super.viewDidLoad()

		let link = "http://speedhunters-wp-production.s3.amazonaws.com/wp-content/uploads/2019/05/11040902/kehala-drift-jump-by-wheelsbywovka-8.jpg"
		
		imageView.sd_setImage(with: URL(string: link), completed: nil)
		
    }
	
    

   
}
