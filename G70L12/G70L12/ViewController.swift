//
//  ViewController.swift
//  G70L12
//
//  Created by Ivan Vasilevich on 5/16/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		NotificationCenter.default.addObserver(self,
											   selector: #selector(displayNumber(notification:)),
											   name: RandomGenerator.randomNumberGeneratedNotification,
											   object: nil)
	}
	
	deinit {
		NotificationCenter.default.removeObserver(self)
	}
	
	@objc func displayNumber(notification: Notification) {
		guard let number = notification.userInfo?["number"] as? Int else {
			return
		}
		title = number.description
	}

	@IBAction func genNumber() {
		RandomGenerator.getRandomNumber()
	}
	
}

class RandomGenerator {
	static let randomNumberGeneratedNotification = NSNotification.Name(rawValue: "randomNumberGeneratedNotification")
	static func getRandomNumber() {
		let number = Int.random(in: 0...35)
		print("randomNumber:", number)
		NotificationCenter.default.post(name: randomNumberGeneratedNotification,
										object: self,
										userInfo: ["number" : number])
	}
}

