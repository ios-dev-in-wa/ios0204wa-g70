//
//  ProfileTVCell.swift
//  G70L10P2
//
//  Created by Ivan Vasilevich on 5/7/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class ProfileTVCell: UITableViewCell {

	@IBOutlet weak var prifileImageView: UIImageView!
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var dateLabel: UILabel!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
