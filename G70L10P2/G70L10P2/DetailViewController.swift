//
//  DetailViewController.swift
//  G70L10P2
//
//  Created by Ivan Vasilevich on 5/7/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

	@IBOutlet weak var detailDescriptionLabel: UILabel!
	
	var detailItem: Date? {
		didSet {
			// Update the view.
			configureView()
		}
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		configureView()
	}
	
	func configureView() {
		// Update the user interface for the detail item.
		if let detail = detailItem {
		    if let label = detailDescriptionLabel {
		        label.text = detail.description
		    }
		}
	}

}

