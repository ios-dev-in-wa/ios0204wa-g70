//
//  RegisterTVC.swift
//  G70L10
//
//  Created by Ivan Vasilevich on 5/7/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class RegisterTVC: UITableViewController {

	@IBOutlet weak var nameTextField: UITextField!
	override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
	}

    // MARK: - Table view data source

	

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if section == 2 {
			return 3
		}
        // #warning Incomplete implementation, return the number of rows
        return super.tableView(tableView, numberOfRowsInSection: section)
    }

	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		if indexPath.row == 2 {
			return 0
		}
		return super.tableView(tableView, heightForRowAt: indexPath)
	}

}
