//
//  SwiftRobotControlCenter.swift
//  MyRobot
//
//  Created by Ivan Vasilevich on 10/4/14.
//  Copyright (c) 2014 Ivan Besarab. All rights reserved.
//
import UIKit
//  All robot commands can be founded in GameViewController.h
class SwiftRobotControlCenter: RobotControlCenter {
	
	/*
	dz1
	
	L2H/L22H - достроить колонны
	
	L2C/L3C - обойти вершины
	
	L1C/L11C - удвоить конфеты на том же месте
	
	L4H/L55H/L555H - шахматка
	
	L4H/L55H/L555H - найти центр мира
	
	методы < 10 строк
	
	*/
	
	func l0c() {
		move()
		doubleMove()
		pick()
		turnRight()
		
		if frontIsClear {
			move()
			turnLeft()
			doubleMove()
			put()
			doubleMove()
		}
		else {
			turnRight()
			turnRight()
		}
	}
	
	func forLoopExample() {
		for _ in 0..<150 {
			put()
			move()
		}
	}
	
	func whileLoopExample() {
		while noCandyPresent {
			if frontIsBlocked {
				break
			}
			move()
		}
	}
	
	//  Level name setup
	override func viewDidLoad() {
		levelName = "L555C" //  Level name
		
		super.viewDidLoad()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		
		super.viewDidAppear(animated)

		
		
		
		turnRight()
		
		
	}
	
	func turnLeft() {
		for _ in 0..<3 {
			turnRight()
		}
	}
	
	func doubleMove() {
		move()
		move()
	}
	
}
