//
//  DataViewController.swift
//  G70L15P3Intro
//
//  Created by Ivan Vasilevich on 5/28/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class DataViewController: UIViewController {

	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var dataLabel: UILabel!
	var dataObject: String = ""


	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		dataLabel!.text = dataObject
		imageView.image = UIImage(named: dataObject)
		
	}


}

